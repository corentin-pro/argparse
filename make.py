#! python3

import glob
import os

from umake import make


class Config:
    CC = 'g++'                                # Compiler to call
    APPS = ['readme_sample', 'test_required'] # Output binaries (need to be found as .cpp directly in SOURCE_DIR)
    JOB_COUNT = int(os.cpu_count() * 0.8)     # Concurent jobs (multi-processing)

    BIN_DIR = 'bin'          # Output directory (binaries)
    INCLUDE_DIR = 'include'  # Include directory (header files)
    OBJECT_DIR = 'obj'       # Temporary directory (object files)
    SOURCE_DIR = 'src'       # Source directories

    COMMON_FLAGS = '-std=c++17'               # Flags used for comiling and linking
    COMMON_DEBUG_FLAGS = '-g'                 # Flags added in debug mode
    COMMON_RELEASE_FLAGS = '-O2 -flto'        # Flags added in release mode
    COMPILE_FLAGS = f'-Wall -I{INCLUDE_DIR}'  # Flags added for compiling (recommandation : `pkg-config --cflags`)
    LINK_FLAGS = ''                           # Flags added for linking (recommandation : `pkg-config --libs`)

    CPP_HEADERS = glob.glob(os.path.join(INCLUDE_DIR, '**', '*.hpp'), recursive=True)
    CPP_SOURCES = glob.glob(os.path.join(SOURCE_DIR, '**', '*.cpp'), recursive=True)


def main():
    make(Config)


if __name__ == '__main__':
    main()
