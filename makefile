MAKE_PATH="."
UMAKE_PATH="umake"

.PHONY: all clean

all:
	@PYTHONPATH=$(UMAKE_PATH) python $(MAKE_PATH)/make.py

debug:
	@PYTHONPATH=$(UMAKE_PATH) python $(MAKE_PATH)/make.py --type debug

clean:
	@PYTHONPATH=$(UMAKE_PATH) python $(MAKE_PATH)/make.py --clean
