from argparse import ArgumentParser


def main():
    parser = ArgumentParser()
    parser.add_argument('arg1')
    parser.add_argument('--arg2', required=True)
    arguments = parser.parse_args()

    print(f'{arguments.arg1=}')
    if 'arg2' in arguments:
        print(f'{arguments.arg2=}')


if __name__ == '__main__':
    main()
