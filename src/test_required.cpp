#include <iostream>

#include "argparse_cpp/argparse.hpp"

using namespace std;

int main(int argc, char** argv)
{
	auto parser = argparse::ArgumentParser();
	parser.add_argument({"arg1"});
	parser.add_argument({"--arg2"}, "", 0, true);
	auto arguments = parser.parse_args(argc, argv);

	cout << "arg1 : " << arguments.getString("arg1") << endl;
	if(arguments.has("--arg2"))
		cout << "arg2 : " << arguments.getString("-a") << endl;

	return 0;
}



