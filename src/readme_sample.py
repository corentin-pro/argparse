from argparse import ArgumentParser


def main():
    parser = ArgumentParser()
    parser.add_argument('arg1', help='This argument is used for X')
    parser.add_argument('-a', '--arg2', help='This optional argument is used for Y')
    arguments = parser.parse_args()

    print(f'{arguments.arg1=}')
    if 'arg2' in arguments:
        print(f'{arguments.arg2=}')


if __name__ == '__main__':
    main()
