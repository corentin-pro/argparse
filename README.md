# Argparse++

Argparse++ is a simple and lightweight argument parsing library for the C++ language, trying to be as close as python's `argparse` as possible.

The library is built in a single header file. For now only the `ArgumentParser` is implemented.


**Note** : `nargs` is not yet implemented


## License

For now the library is using the BSD-3-Clause license, it is integrated in the header file so it can be used as is. As a reminder this license allow all uses but prevent using any trademark (name, company) to endorse your product not hold for any liability.


## Installation

Simply copy the `argparse.hpp` file in your include directory or add as a submodule (can be updated easily) :

```
git submodule add https://gitlab.com/corentin-pro/argparse_cpp.git
```


## Usage

Compared to python's `argparse`, the return arguments need to be retrieve through `getString`, `getBool`, `getInt` or `getFloat` and in case of multiple arguments names/flags any of them can be used (see sample).

The `add_argument` prototypes are :

```cpp
void add_argument(
	const std::vector<std::string>& names,
	const std::string& help,
	const std::any& default_value,
	const bool required)

void add_argument(
	const std::vector<std::string>& names,
	const std::string& help="",
	const Action& action=Action::store,
	const std::variant<int, char, Nargs>& nargs=Nargs::optional,
	const std::any& default_value={},
	const std::any& const_value={},
	const bool required=false)
```

Sample :

```c++
#include <iostream>

#include "argparse.hpp"

using namespace std;

int main(int argc, char** argv)
{
	auto parser = argparse::ArgumentParser();
	parser.add_argument({"arg1"}, "This argument is used for X");
	parser.add_argument({"-a", "--arg2"}, "This optional argument is used for Y");
	auto arguments = parser.parse_args(argc, argv);

	cout << "arg1 : " << arguments.getString("arg1") << endl;
	if(arguments.has("arg2"))
		cout << "arg2 : " << arguments.getString("a") << endl;

	return 0;
}

```
